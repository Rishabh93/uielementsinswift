//
//  MyButtonClass.swift
//  UIElementsInSwift
//
//  Created by Rishabh  on 01/06/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

import Foundation
import UIKit
class MyButtonClass: UIButton {
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        backgroundColor = UIColor.blueColor()
//    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.blueColor()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
