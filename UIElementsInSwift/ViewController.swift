//
//  ViewController.swift
//  UIElementsInSwift
//
//  Created by Rishabh  on 30/05/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIAlertViewDelegate,UITextFieldDelegate {

//    @IBOutlet weak var myLabel: UILabel!
//    @IBOutlet weak var myButton: UIButton!
    @IBOutlet weak var myImage: UIImageView!
    var newCenter : CGPoint!
    @IBOutlet weak var mySegmentedController: UISegmentedControl!
    @IBOutlet weak var myTextField: UITextField!
    @IBOutlet weak var mySlider: UISlider!
    @IBOutlet weak var labelForSlider: UILabel!
    @IBOutlet weak var myProgessView: UIProgressView!
    @IBOutlet weak var myActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var mySwitch: UISwitch!
//    @IBOutlet weak var myButton : MyButtonClass!

//    @IBOutlet weak var myLabel : MyLabelClass!
    var myButton:MyButtonClass!
    var myLabel:MyLabelClass!
    override func viewDidLoad() {
        super.viewDidLoad()
        myActivityIndicator.startAnimating()
        myLabel = MyLabelClass(frame: CGRectMake(100, 100, 200, 50))
        myLabel.text = "Rishabh Gupta"
        view.addSubview(myLabel)
        myLabel.userInteractionEnabled = true
        newCenter = myLabel.center
        let panGesture = UIPanGestureRecognizer(target: self, action: (#selector(ViewController.funcForLabel(_:))))
      //gesture.addTarget(self, action:#selector(ViewController.funcForLabel(_:)))
        myLabel.addGestureRecognizer(panGesture)
        myButton = MyButtonClass(frame: CGRectMake(100,175, 100, 50))
        myButton.setTitle("Rishabh", forState: .Normal)
        myButton.userInteractionEnabled = true
        view.addSubview(myButton)
//        myButton.addTarget(self, action: #selector(ViewController.myFunc), forControlEvents: UIControlEvents.TouchUpInside)
        let longPressed = UILongPressGestureRecognizer(target: self, action: (#selector(ViewController.longPressFunc(_:))))
        myButton.addGestureRecognizer(longPressed)
        myImage.userInteractionEnabled = true
        
        for index in 1 ..< 3 {
            print(index)
            let tapGesture = tapGestureRecongnizerForNumberOfTouches(index)
            myImage.addGestureRecognizer(tapGesture)

        }

        
//        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(ViewController.funcForPinch(_:)))
//        view.addGestureRecognizer(pinchGesture)
    }
    
//  ---------------------------------------------------------------------------------------------------------
    
    func funcForLabel(gestureRecognizer:UIPanGestureRecognizer) {
    
//      var newLabel = gestureRecognizer.view
        let point = gestureRecognizer.translationInView(myLabel)
        myLabel.center = CGPointMake((myLabel.center.x) + point.x, (myLabel.center.y) + point.y)
        gestureRecognizer.setTranslation(CGPointZero, inView: myLabel)       //it will not stick where u leave
        if(gestureRecognizer.state == UIGestureRecognizerState.Began) {      //it will called only once
            print(myLabel.center)
            print("Hello")
        }
        if(gestureRecognizer.state == UIGestureRecognizerState.Ended) {     //it will called only once
            myLabel.center = newCenter
            print("Bye")
        }
        
    }
    
    func tapGestureRecongnizerForNumberOfTouches(numberOfTouches : Int) -> UITapGestureRecognizer {
        let tapGesture = UITapGestureRecognizer(target: self, action: (#selector(ViewController.funcForImage(_:))))
        tapGesture.numberOfTouchesRequired = numberOfTouches
        return tapGesture
    }
//  ---------------------------------------------------------------------------------------------------------
    func longPressFunc(gestureRecognizer:UILongPressGestureRecognizer) {
        if(gestureRecognizer.state == UIGestureRecognizerState.Ended) {
//            let alert : UIAlertView = UIAlertView(title: "Title", message: "Message", delegate: self, cancelButtonTitle: "OK", otherButtonTitles : "Continue", "Continue1")
            
            
            let alert = UIAlertController(title: "Button Pressed", message: "Long Press", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive, handler: {(action : UIAlertAction) -> Void in
            
            }))
            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.Default, handler: {(action : UIAlertAction) -> Void in
                print("Pressed on Continue")
            }))
            alert.addAction(UIAlertAction(title: "Continue1", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
//        alert.show()
        }
    }
    //-------------------------------------------------------------------------------------------------------
    
    func funcForImage(gestureRecognizer:UITapGestureRecognizer) {
        
        if(gestureRecognizer.state == UIGestureRecognizerState.Ended) {
        let alert = UIAlertController(title: "Image Tapped", message: "ImageView Tapped \(gestureRecognizer.numberOfTouchesRequired)", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    //--------------------------------------------------------------------------------------------------------
    
    func funcForPinch(sender : UIPinchGestureRecognizer) {
        sender.view?.transform = CGAffineTransformScale((sender.view?.transform)!, sender.scale,sender.scale)
        sender.scale = 1
    }
    //-------------------------------------------------------------------------------------------------------
    
    @IBAction func funcForSegController(sender: AnyObject) {
        if mySegmentedController.selectedSegmentIndex == 0 {
            let alert = UIAlertController(title: "Segmented Control", message: "First", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }else if mySegmentedController.selectedSegmentIndex == 1 {
            let alert = UIAlertController(title: "Segmented Control", message: "Second", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }else {
            let alert = UIAlertController(title: "Segmented Control", message: "Third", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    //------------------------------------------------------------------------------------------------------
    
    @IBAction func funcForTextField(sender: AnyObject) {
        let alert = UIAlertController(title: "Text Field Editing", message: "Complete", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    func textFieldDidEndEditing(textField: UITextField) {
        let alert = UIAlertController(title: "Text Field Editing", message: "Complete", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    //------------------------------------------------------------------------------------------------------
    
    @IBAction func funcForSlider(sender: AnyObject) {
        mySlider.minimumValue = 0.0
        mySlider.maximumValue = 100.0
        let sliderValue : Float = sender.value
        let sliderValue1 :String = String(sliderValue)
        labelForSlider.text = sliderValue1
        myProgessView.setProgress(sliderValue/100, animated: true)
    }
    //-------------------------------------------------------------------------------------------------------
    @IBAction func funcForSwitch(sender: AnyObject) {
        if mySwitch.on == true {
        let alert = UIAlertController(title: "Switch", message: "ON", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Switch", message: "OFF", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }

    }
//    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
//        
//    }
//    func myFunc() {
//        print("Touch up inside")
//    }
    
//    @IBAction func funcForButton(sender: UIButton) {
//        let alert = UIAlertController(title: "My Title", message: "Message", preferredStyle: UIAlertControllerStyle.Alert)
//        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
//        alert.show()
//    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

